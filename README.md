This project was conducted within my summer internship at Laevitas : https://www.laevitas.ch/.

Laevitas is a company working on analytics for the crypto derivatives market.

Due to confidentiality reasons, the code written to generate the results mentioned in the report cannot be displayed in this repo. Nevertheless I will leave the links for the final work on laevitas plateform, since it has already been deployed. Enjoy !

Squeeth is a derivative product that has Ethereum as underlying.

Crab strategy is an automated trading strategy based on squeeth and Ethereum.

Squeeth PnL calculator : https://app.laevitas.ch/defi/squeeth-tool

Hedging vanilla options with squeeth : https://app.laevitas.ch/defi/hedging

Crab strategy simulator : https://app.laevitas.ch/defi/crab
